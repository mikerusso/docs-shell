#!/bin/bash

### Functions ###

# Navigation #
source functions/assets/help.sh

# Alerts #
source functions/assets/alerts.sh

# Status #
source functions/assets/status.sh

# Git #
source functions/assets/git.sh

# Navigation #
source functions/assets/navigation.sh

# Docs compile #
source functions/compile/docs-compile.sh
source functions/compile/update.sh
source functions/compile/pull.sh

# Lint #
source functions/lint/lint.sh
source functions/lint/gitlab-docs.sh
source functions/lint/content.sh
source functions/lint/docs-maintenance.sh

# Git usage functions #
source functions/git/branching.sh
source functions/git/ops.sh

# Test this project
source functions/assets/test.sh

# Installation
source install/install.sh
source install/dependencies.sh
source install/update-dep.sh
source install/ruby.sh
source install/clone.sh
source install/clone-temp.sh
source install/paths.sh
source install/linters.sh
source install/symlinks.sh
source install/pre-install.sh
source install/gitlab-dep.sh
source install/gdk.sh
source install/ssh_gitlab.sh
source install/update-docs-shell.sh
