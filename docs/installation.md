# Install GitLab Docs Shell

To build, preview, and test GitLab Docs locally, you need to have on your
computer all [repos](https://gitlab.com/gitlab-org/gitlab-docs#projects-we-pull-from)
GitLab Docs pull from and install all its dependecies. You'll also have to
update them regularly to maintain your copy.

If done manually, it is a tedious and error-prone process. Docs
Shell was built to ease the installation process and simplify its maintance.

During the installation process, Docs Shell will prompt you to either clone
all repos or to set their paths if you already have them cloned on your
computer.

## Installation process

1. Create a directory in your computer to clone Docs Shell onto according to [file tree](#file-tree).
1. Clone Docs Shell:
   1. For [GitLab team members](#for-gitlab-team-members).
   1. For [external contributors](#for-external-contributors).
1. [Install Docs Shell](#install-docs-shell).

### File tree

The file tree Docs Shell considers **default** is:

```text
.
├── main-dir
│   ├── docs-shell
│   ├── gitlab-docs
│   ├── gitlab
│   ├── omnibus-gitlab
│   ├── gitlab-runner
│   └── charts
```

`main-dir` can be any directory of your choice.

When you choose to clone all repos with Docs Shell, they will be cloned
according to the default file tree.

If you have a different file tree, you only need to clone Docs Shell into a folder of your choice:

```text
.
├── main-dir
│   ├── docs-shell
```

The path to the other repos can be set during the [installation process](#install-docs-shell).

**Note:** If you have any problems or questions before and during the
installation process, see [installation details](docs/installation.md).

### Clone Docs Shell

The first step of the installation process is to clone Docs Shell onto
your computer.

#### For GitLab team members

If you are a GitLab team member, start by cloning this project (Docs Shell)
onto your computer:

1. Create a folder in your computer according to the [file tree](#file-tree)
   (for example, `main-dir`) and [open it in a terminal window](workflows.md#open-a-directory-in-a-terminal-window):

   ```shell
   cd main-dir
   ```

1. [Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)
your fork of the Docs Shell project onto your computer into the `main-dir`:

   ```shell
   git clone git@gitlab.com:gitlab-org/docs-shell.git
   ```

   If you don't have SSH set for your GitLab account, clone it via https:

   ```shell
   git clone https://gitlab.com/gitlab-org/docs-shell.git
   ```

   When you start the Docs Shell installation, you will be prompted to set up SSH.

You're ready to [install Docs Shell](#install-docs-shell).

#### For external contributors

If you are **not** a GitLab team member, the process is similar, but you'll
need to fork the repositories and clone them yourself **before installing**
Docs Shell.

1. From the GitLab UI, [fork](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#fork)
Docs Shell into your namespace.
1. Create a folder in your computer to hold all repos (for example, `main-dir`) and [open it in a terminal window](workflows.md#open-a-directory-in-a-terminal-window).
1. [Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)
your fork of the Docs Shell project onto your computer into the `main-dir`.
1. Repeat the process (fork and clone) the following repos into your `main-dir`:
   - [GitLab Docs](https://gitlab.com/gitlab-org/gitlab-docs)
   - [GitLab](https://gitlab.com/gitlab-org/gitlab)
   - [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner)
   - [Omnibus GitLab](https://gitlab.com/gitlab-org/omnibus-gitlab)
   - [GitLab Charts](https://gitlab.com/gitlab-org/charts/gitlab)

This can take quite some time. Make sure you've done this before starting the installation process for Docs Shell.

Once done, you're ready to [install Docs Shell](#install-docs-shell).

### Install Docs Shell

1. [`cd`](docs/workflows.md#open-directory-in-a-terminal-window) to the `docs-shell` directory in a terminal window:

   ```shell
   cd docs-shell
   ```

1. Grant terminal execution to Docs Shell:

   ```shell
   . ./docs.sh && access
   ```

1. Test the access:

   ```shell
   docs hello
   ```

   The expected output is one single line saying hello to you.

1. Install Docs Shell. You'll be prompted to either clone all repos or enter the local paths to exisiting repos. Follow the prompt steps:

   ```shell
   docs install
   ```

1. If installed successfully, Docs Shell will open a new browser tab or window previewing GitLab Docs. Give it a few seconds to load. Yay!

If anything goes wrong during the installation process, quit the terminal and open it again, then run `docs install`.

See [**installation details**](installation-details.md) for further info and troubleshooting.

Once installed, give it a try to see how it works:

1. Open any GitLab document (`gitlab/doc/<choose-a-file>.md`) and make a change.
1. Run `docs recompile`. Refresh your browser to see the changes.
1. Open another terminal window (wherever) and run `docs lint -c gitlab` to lint `markdownlint` and `vale` in `gitlab`. Check the output.
1. To stop the live preview process, or to stop the script at any moment,
press <kbd>control</kbd> + <kbd>C</kbd> on your keyboard. You can also run `docs kill` to stop Nanoc live.
