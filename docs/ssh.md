# Set up SSH authentication for you GitLab.com account with Docs Shell

If you're not familiar with [SSH authentication](https://docs.gitlab.com/ee/ssh/),
it's easy to set it up for your GitLab.com account with Docs Shell.

You'll be prompted to set it up when cloning all repos during the Docs Shell
installation process.

If you choose not to set it at that time, you'll have to clone all repos
manually. But you can set up SSH later, either [manually](https://docs.gitlab.com/ee/ssh/#generating-a-new-ssh-key-pair)
or by running `docs ssh-gitlab` to generate a new key pair as explained below.

## Generate new SSH key pair

After installing Docs Shell:

1. Open a terminal window and run `docs ssh-gitlab`.
1. Docs Shell will generate an SSH key pair with `ed25519` encryption.
1. You'll be prompted to enter the email address associated with your GitLab account - it will be added as a key label.
1. You'll be prompted to enter a passphrase (password) for the new key. You'll need it later to log into GitLab through SSH.
1. Docs Shell will copy the public SSH key and open the browser window where you have to paste it.
1. Paste it into the **Key** field and choose an expiring date.
1. Back to your Docs Shell terminal, continue the script.

Remember this password as you need it later. See [renew SSH](#renew-ssh) for
details. But if you don't remember, run the script again and Docs Shell will
create another key pair for you.

At any moment, if you want to generate a new key pair (even if you already have a key pair working) run `docs new-ssh-key`.

## Renew SSH

There are some occasions where you'll need to re-enter the password for your
key. One of them is when you shut down your computer. Any attempt of pulling or
pushing to a repository will output the error:

```shell
git@gitlab.com: Permission denied (publickey,keyboard-interactive).
fatal: Could not read from remote repository.
```

To resolve this:

1. Run `docs renew-ssh`.
1. Enter the password to your key (generated through the steps above).

You're ready to communicate with GitLab again.

If you don't remember the password, run `docs ssh-gitlab` and Docs Shell will
generate another key pair for you.
