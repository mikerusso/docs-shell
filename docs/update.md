# Update Docs Shell

Docs Shell was transferred from its [original repo](https://gitlab.com/marcia/docs-shell/) to the [`gitlab-org`](https://gitlab.com/gitlab-org/)
group.

To update Docs Shell:

- If your local copy of Docs Shell was made from the `gitlab-org` repo, use the [regular update](#regular-update).
- If you had installed Docs Shell from the original repo, you need to
[reinstall](#reinstall-to-update) it. The original project
is deprecated and will no longer be updated.

## Regular update

To update Docs Shell:

1. Open `docs-shell` in a terminal window.
1. Run `docs shell --update`.

   If you use a fork of Docs Shell, run `docs shell --update --fork` to fetch `upstream` and push to `origin`.

## Reinstall to update

To update Docs Shell to the new repo in `gitlab-org`, you can opt to either
reinstallation methods:

- [Start over](#reinstall-by-starting-over) (preferred).
- [Update your current installation](#reinstall-from-existing-installation).

[Reinstallation options](#reinstallation-options) are valid for both methods.

You can also [reinstall Docs Shell from another directory](#reinstall-in-a-different-docs-shell-directory) at any time.

### Reinstall by starting over

To wipe out your current Docs Shell installation and reinstall it:

1. Delete your old `docs-shell` directory.
1. Open a terminal window to clone Docs Shell into.
1. Clone Docs Shell either via:

   - SSH: `git clone git@gitlab.com:gitlab-org/docs-shell.git`.
   - HTTPS: `git clone https://gitlab.com/gitlab-org/docs-shell.git`.

1. Go to the Docs Shell directory: `cd docs-shell`.
1. Run `. ./docs.sh && reinstall`.

See also other [options available for `docs reinstall`](#reinstallation-options).

### Reinstall from existing installation

To keep your current Docs Shell installation, reinstall it with this method.

#### 1. Update the repo's remote origin

First update your local repository origin:

1. Open your local Docs Shell directory in a terminal window:

   ```shell
   cd docs-shell
   ```

1. Check the remote repos linked to your local repo:

   ```shell
   git remote -v
   ```

1. Remove the remote old origin and add the new one:

   ```shell
   git remote remove origin
   git remote add origin git@gitlab.com:gitlab-org/docs-shell.git
   ```

From then on, when you pull and push to `origin`, Git will connect you directly
to the project in `gitlab-org`.

#### 2. Update your local repo

With the new `origin` set, reset your local copy with the remote `origin`.

**Warning!** This process:

- Discards any changes you have in your local copy of GitLab Docs Shell.
- Wipes your Docs Shell data (variables and paths).

1. Open Docs Shell in a terminal window.
1. Fetch the new origin:

   ```shell
   git fetch origin
   ```

1. Reset your local copy:

   ```shell
   git reset --hard origin/master
   ```

#### 3. Reinstall Docs Shell

Now that you updated your local copy with the latest remote copy, reinstall Docs Shell.

1. Open `docs-shell` in a terminal window
1. Reinstall Docs Shell:

   ```shell
   docs reinstall
   ```

See also other [options available for `docs reinstall`](#reinstallation-options).

## Reinstallation options

When reinstalling Docs Shell you can use flags for the following options:

- To clone all repos again (GitLab Docs, GitLab, Runner, Omnibus,
  Charts), pass the flag `--clone` to `reinstall`: `docs reinstall --clone` or `. ./docs.sh && reinstall --clone`.
- To preview GitLab Docs immediately after the reinstallation, pass the flag `--start` to `reinstall`: `docs reinstall --start` or `. ./docs.sh && reinstall --start`.

You can only pass **one** of the flags.

## Reinstall in a different `docs-shell` directory

At any moment, if you want to reinstall in a different `docs-shell` directory:

1. Move the `docs-shell` directory to the new location.
1. Open a terminal window in the new `docs-shell` directory.
1. Run `. ./docs.sh && reinstall`.

See also other [options available for `docs reinstall`](#reinstallation-options).
