# Help

--help(){
echo "`tput bold`# Docs Shell usage #`tput sgr0`

`tput bold`- Install Docs Shell:`tput sgr0`
  $ docs access                                               # Grant read/execute permissions to Docs Shell
  $ docs install                                              # Check and install GitLab Docs' dependencies
  $ docs reinstall                                            # Reinstall Docs Shell
  $ docs reinstall --clone                                    # Reinstall Docs Shell and clone all repos

`tput bold`- Test Docs Shell installation:`tput sgr0`
  $ docs hello                                                # Test docs command
  $ docs test files                                           # Test linked files

`tput bold`- Check all the dependencies:`tput sgr0`
  $ docs dep                                                  # Check dependencies
  $ docs dep --update                                         # Check and update dependencies

`tput bold`- Compile GitLab Docs:`tput sgr0`
  $ docs compile                                              # Checkout GitLab Docs' default branch, pull, compile docs

`tput bold`- Recompile GitLab Docs:`tput sgr0`
  $ docs recompile                                            # Recompile docs

`tput bold`- Live preview GitLab Docs:`tput sgr0`
  $ docs live                                                 # Live-preview docs

`tput bold`- Live preview GitLab Docs:`tput sgr0`
  $ docs live                                                 # Live-preview docs

`tput bold`- Linters:`tput sgr0`
`tput bold`- Content linters:`tput sgr0`
  $ docs lint                                                 # Content (all repos, all linters), and Nanoc (all linters)
  $ docs lint -c | --content                                  # Content only (all repos, all content linters)
  $ docs lint -c [repo]                                       # Content, specific repo, all linters
                                                                Repos: [gitlab], [omnibus], [runner], [charts]
  $ docs lint -c [repo] [content linter]                      # Content, specific repo, and specific linter
                                                                Content linters: [markdown], [vale]
  $ docs lint -c gitlab views                                 # Content linter available for GitLab only

`tput bold`- Nanoc linters:`tput sgr0`
  $ docs lint -n | --nanoc                                    # Nanoc only (all linters)
                                                                Nanoc linters: [links], [anchors], [elinks]
  $ docs lint -n [linter]                                     # Nanoc, specific linter
  $ docs lint -c [repo] [content linter] -n [nanoc linter]    # Content (specific repo and specific linter) + Nanoc (all linter or specific linter)

`tput bold`- GitLab Docs' development linters:`tput sgr0`
  $ docs lint -d | --dev                                      # Development, all linters
  $ docs lint -d [linter]                                     # Development, specific linter
                                                                Development linters: [jest], [scss], [yaml], [yarn], [rspecs], [ci]

`tput bold`- Troubleshooting:`tput sgr0`
`tput bold`- Reset repos:`tput sgr0`
  $ docs reset [repo]                                       # Content, specific repo, all linters
                                                              Repos: [gitlab], [omnibus], [runner], [charts], [docs], [shell], [gdk]

`tput bold`- Navigation:`tput sgr0`
  $ docs go_[repo]                                            # Navigate to repo
                                                                Repos: [gitlab], [omnibus], [runner], [charts], [shell], [docs], [gdk]

`tput bold`- GDK:`tput sgr0`
  $ docs gdk-set                                              # Set up GDK
  $ docs gdk-update                                           # Update GDK
  $ docs gdk-update --min                                     # Minimal GDK update
  $ docs gdk-lint-fe                                          # Lint gdk/gitlab's frontend (.vue and .js files)
  $ docs gdk-lint-fe --write                                  # Pass the flag -w for prettier --write
"}

