# Test files (docs test files)
test_update() {
  echo 'functions/compile/update.sh ok'
}

# ---------------------------------------------------------------- #

# `docs update` functions #

# Usage: docs update; docs update -s (stash EE); docs update --skip (skip EE)
# docs update -h (housekeeping), docs update -h -s (housekeeping + stash EE);
# docs update -a (all repos quick); docs update -h -a (housekeeping all)
update() {
  # Pull GitLab
  if [[ $1 == '--skip' ]]; then
    tput setaf 2; echo 'GitLab skipped! \n'; tput sgr0
  else
    tput setaf 5; echo 'Pulling GitLab'
    go_gitlab
    if [[ $1 == '-h' ]]; then
      if [[ $2 == '-s' ]]; then
        git_stash_checkout_master
        echo 'git_stash_checkout_master'
      else
        git_update_house
      fi
    elif [[ $1 == '-s' ]]; then
      git_stash_checkout_master
    else
      git_update_quick
    fi
    tput setaf 2; echo 'GitLab done! \n'; tput sgr0
  fi

  # Pull Omnibus
  tput setaf 5; echo 'Pulling Omnibus'
  go_omnibus
  if [[ $1 == '-h' ]]; then
    git_update_house
  else
    git_update_quick
  fi
  tput setaf 2; echo 'Omnibus done! \n'; tput sgr0

  # Pull Runner
  tput setaf 5; echo 'Pulling Runner'
  go_runner
  if [[ $1 == '-h' ]]; then
    git_update_house
  else
    git_update_quick
  fi
  tput setaf 2; echo 'Runner done! \n'; tput sgr0

  # Pull Charts
  tput setaf 5; echo 'Pulling Charts'
  go_charts
  if [[ $1 == '-h' ]]; then
    git_update_house
  else
    git_update_quick
  fi
  tput setaf 2; echo 'Charts done! \n'; tput sgr0

  # Pull GitLab Docs
  if [[ $1 == '-h' ]]; then
    if [[ $2 == '-a' ]]; then
      tput setaf 5; echo 'Pulling Docs'
      go_docs
      git_update_house
      tput setaf 2; echo 'GitLab Docs done! \n'; tput sgr0
    fi
  fi
  if [[ $1 == '-a' ]]; then
    #tput setaf 5; echo 'Pulling GitLab Docs'
    #go_docs
    #git_update_quick
    #tput setaf 2; echo 'GitLab Docs done! \n'; tput sgr0
  fi
  yay
}
