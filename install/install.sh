# Script for ZSH

# Test files (docs test files)
test_install() {
  echo 'install/install.sh ok'
}

# Test function
hello() {
  printf "Hello $USER! $WAVE \n"
}

# ---------------------------------------------------------------- #

# Install Docs Shell

# Initial command: `. ./docs.sh && access`

# Access
access(){
  CURRENT=$(pwd)
  tput setaf 5 ; printf "Granting shell access to Docs Shell... \n" ; tput sgr0
  echo "Granting terminal execution... \n`tput bold`$ chmod +x $CURRENT/docs.sh`tput sgr0`" ; chmod +x $CURRENT/docs.sh
  echo "Exporting path to terminal... \n`tput bold`$ export PATH=$PATH:$CURRENT`tput sgr0`" ; export PATH=$PATH:$CURRENT
  echo "Done."
  # Source temporary alias
  source functions/assets/alias.zsh
  # Add permanent alias
  if [[ $SHELL == '/bin/zsh' ]]; then
    tput setaf 5 ; printf "Adding a permanent alias... \n" ; tput sgr0
    perm_alias
  else
    set_shell
  fi
}

# Permanent alias
perm_alias() {
  PERMALIAS=$(echo "" ; echo "# Docs Shell" ; echo 'alias docs="cd '$CURRENT' && . ./docs.sh && "' && echo "")
  if [[ $SHELL == '/bin/zsh' ]]; then
    if [ -e ~/.zshrc ] ; then
      # If there's already a Docs Shell alias, remove it
      if ( grep "alias docs" ~/.zshrc >/dev/null) ; then
        sed -i '' "/Docs Shell/d" ~/.zshrc
        sed -i '' "/alias docs/d" ~/.zshrc
        echo "Existing alias removed from ~/.zshrc."
      fi
    fi
    printf "\n$PERMALIAS" >> ~/.zshrc
    source ~/.zshrc
    echo "Permanent alias added to ~/.zshrc"
  else
    error_sound
    tput setaf 1 ; echo "ZSH is a hard requirement for Docs Shell." ; tput sgr0
  fi
}

# Check ZSH
set_shell(){
  if grep -q 'zsh' /etc/shells ; then
    echo "ZSH is installed but not the default shell."
    zsh
    ok_sound ; read "REPLY?`tput setaf 3`(Recommended) Set ZSH as the default shell (y/n)?"`tput sgr0`
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      echo "$ chsh -s /bin/zsh" ; chsh -s /bin/zsh
      tput setaf 5 ; printf "Adding a permanent alias... \n" ; tput sgr0
      perm_alias
    else
      echo "This might cause unforeseen errors. If you change your mind, run `tput bold`$ chsh -s /bin/zsh`tput sgr0` to make it the default shell."
      tput setaf 5 ; printf "Adding a permanent alias to ZSH... \n" ; tput sgr0
      perm_alias
    fi
  else
    error_sound
    echo "`tput setaf 1`Houston, we have a problem! ZSH isn't installed.`tput sgr0`." ; error_sound
    read "REPLY?`tput setaf 3`(Required) Would you like install ZSH now (y/n)?`tput sgr0`"
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      pre-install
      tput setaf 5 ; printf "Adding a permanent alias... \n" ; tput sgr0
      perm_alias
    else
      echo "ZSH is a hard requirement for Docs Shell. Using a different shell might cause unforeseen errors."
      echo "You can install ZSH later by running `tput bold`$ brew install zsh`tput sgr0`. Then restart the installation process from zero."
      tput setaf 1 ; echo "Aborting." ; tput sgr0
      return false
    fi
  fi
}

# Compile and preview docs
first_compile(){
  cd $DSHELL
  docs_bundler
  check_current_docs_ruby
  go_docs
  echo `tput setaf 5`"Bundling GitLab Docs' dependecies..." `tput sgr0`
  echo `tput bold`"$ bundle "_"$DBUN"_" install"`tput sgr0` ; bundle '_'$DBUN'_' install
  echo `tput setaf 5`"Compiling the docs site..." `tput sgr0`
  echo `tput bold`"$ bundle exec nanoc compile" `tput sgr0`; bundle exec nanoc compile
  ifsuccess
  echo `tput setaf 5`"Starting live preview..." `tput sgr0`
  echo `tput bold`"$ bundle exec nanoc live -p $PORT" `tput sgr0`; bundle exec nanoc live -p $PORT | open_preview
}

# ---------------------------------------------------------------- #

# Install Docs Shell: `docs install`

install() {
  remove_variables
  unset DGDK
  unset CLONE
  printf "Hello `tput setaf 5`$USER`tput sgr0`! ${WAVE}\n"
  echo "Before we begin the installation process, make sure your terminal is open in the docs-shell directory:\nIt's in `tput bold`$(pwd).`tput sgr0`\n"
  echo "If it's not the correct folder, abort the process by pressing 'control' + 'c' on your keyboard, then"
  echo "run `tput bold`$ docs install`tput sgr0` when you're ready.\n"
  # Clone repos or setup current paths and create symlinks
  ok_sound ; tput setaf 3 ; read "REPLY?Do you have `tput bold`all repos already cloned`tput sgr0``tput setaf 3` (GitLab, Omnibus, Runner, Charts, Docs) (y/n)?`tput sgr0`"
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      paths
    else
      ok_sound ; tput setaf 3 ; read "REPLY?Are you a GitLab team member (y/n)?`tput sgr0`"
        if [[ $REPLY =~ ^[Yy]$ ]] ; then
          clone_temp
          CLONE='0'
        else
          echo "Please clone all the repos first. For details, see https://gitlab.com/gitlab-org/gitlab-docs#clone-the-repositories."
          return
        fi
    fi
  # Dependencies
  # Check GDK Ruby
  if [[ $DGDK == '0' ]]; then
    gdk_ruby
  else
    ask_install_gdk
  fi
  # Check all dependencies
  echo `tput setaf 5`"Dependencies check:" `tput sgr0`
  check_dependencies
  if [[ $? == 0 ]] ; then
    tput setaf 2; printf "${UNICORN} Dependencies are ready! `tput sgr0` (${NOW}) \n"
  fi
  # Installation log file
  echo "Installed on ${NOW}" > $DSHELL/.log
  # Set up upstream project
  echo `tput setaf 5`"Checking upstream..." `tput sgr0`
  check_upstream
  # Pull if not cloned
  if [[ $CLONE != '0' ]]; then
    # Pull all repos
    echo `tput setaf 5`"Pulling all repos..." `tput sgr0`
    pull
  fi
  # Compile and preview
  echo `tput setaf 5`"Compiling GitLab Docs..." `tput sgr0`
  first_compile
}

# ---------------------------------------------------------------- #

# Install GDK

ask_install_gdk() {
  # Set GDK
  ok_sound
  read "REPLY?`tput setaf 3`Would you like set up GDK with Docs Shell (you must have GDK already cloned and installed) (y/n)?`tput sgr0`"
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    gdk_set_path
    gdk_ruby
  else
    echo "When you install GDK, run `tput bold`$ docs gdk-set`tput sgr0` to set it up with Docs Shell."
  fi
}
